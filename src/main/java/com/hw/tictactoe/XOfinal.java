/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hw.tictactoe;

/**
 *
 * @author BenZ
 */
import java.util.*;

public class XOfinal {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char player = 'X';
    static int row, col;
    static boolean isFinish = false;
    static char winner = '-';
    static int draw = 0;

    static void showWelcome() {
        System.out.println("Welcome to XO Game");
    }

    static void showTable() {
        System.out.println("  1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    static void showturn() {
        System.out.println(player + " turn");
    }

    static void input() {

        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.println("Please input Row Col :");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table[row][col] == '-') {
                table[row][col] = player;
                draw++;
                break;
            }
            System.out.println("Error : table at row and col is not empty!!!!");
        }

    }

    static void checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row][i] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkCross1() {
        for (int i = 0, j = 2; i < 3; i++, j--) {
            if (table[j][i] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkCross2() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkDrew() {
        if (draw == 9 && winner == '-') {
            winner = 'd';
            isFinish = true;
        }

    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkCross1();
        checkCross2();
        checkDrew();

    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if (winner == 'd') {
            System.out.println("Draw!!!");
        } else {
            System.out.println(winner + " Win");
        }
    }

    static void showBye() {
        System.out.println("Bye Bye....");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showturn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showTable();
        showResult();
        showBye();
    }

}
